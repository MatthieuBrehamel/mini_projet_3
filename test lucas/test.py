import sqlite3
import csv
import os
import logging
import argparse

#Création du fichier logging
logging.basicConfig(filename='import_base.log',level=logging.DEBUG)

fichier = open('voiture.csv',"r")


conn = sqlite3.connect('example.db')

c = conn.cursor()
 
#test si la table existe deja
c.execute(''' SELECT count(name) FROM sqlite_master WHERE type='table' AND name='automobile'; ''')
#si la table existe on log
if c.fetchone()[0]==1 : 
	logging.warning('La table automobile existe déjà')
else:
    #si la table n'existe pas on la crée
    c.execute('''CREATE TABLE automobile
             (adresse_titulaire text,nom text,prenom text,immatriculation text,date_immatriculation text,vin text,marque text,denomination_commerciale text,
             couleur text,carrosserie text,categorie text,cylindree text,energie text,places text,poids text,puissance text,type text,variante text,version text)''')
    logging.info('Table automobile créé')

reader = csv.reader(fichier)

for row in reader:
	row = fichier.split(';')
    print(row)


conn.commit()

conn.close()
